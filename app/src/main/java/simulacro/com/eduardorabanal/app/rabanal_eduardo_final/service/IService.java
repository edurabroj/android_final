package simulacro.com.eduardorabanal.app.rabanal_eduardo_final.service;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.entities.Actividad;

/**
 * Created by USER on 12/07/2017.
 */
public interface IService {

    @FormUrlEncoded
    @POST("crear")
    Call<ResponseBody> Crear
    (
        @Field("date") String date,
        @Field("activity") String activity,
        @Field("assigned") String assigned
    );

    @GET("{codigo}")
    Call<List<Actividad>> GetActividades(@Path("codigo") String codigo);

    @POST("{id}/done")
    Call<ResponseBody> Ejecutar (@Path("id") int id);
}
