package simulacro.com.eduardorabanal.app.rabanal_eduardo_final.adapters;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.R;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.Statics;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.activities.MainActivity;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.entities.Actividad;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.service.IService;

/**
 * Created by USER on 12/07/2017.
 */
public class AdapterLV extends BaseAdapter {
    private MainActivity actividad;
    private IService service;

    public void setActivity(MainActivity activity) {
        this.actividad = activity;
    }

    public void setService(IService service) {
        this.service = service;
    }

    private Context context;
    private List<Actividad> dataset;

    public AdapterLV() {
        dataset=new ArrayList<>();
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return dataset.size();
    }

    @Override
    public Object getItem(int position) {
        return dataset.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_lv, parent, false);
        }

        final Actividad a = (Actividad) getItem(position);

        final TextView activity = (TextView) convertView.findViewById(R.id.activity);
        activity.setText(a.getActivity()+"\n"+a.getDate());

        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);

        Date now = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date fecha = new Date();
        try {
            fecha =  df.parse(a.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String url;
        if(a.isDone()==1)
        {
            url = Statics.caritaFeliz;
        }else
        {
            //activity.setText(a.getActivity()+"\n"+a.getDate()+"\n"+df.format(now));
            fecha.setHours(23);
            fecha.setMinutes(59);
            if(fecha.after(now)){
                url = Statics.caritaNeutral;
            }
            else
            {
                url = Statics.caritaTriste;
            }
        }


        Glide.with(context)
                .load(url)
                .into(imageView);

        CheckBox ejecutar = (CheckBox) convertView.findViewById(R.id.btnEjecutar);
        ejecutar.setChecked(a.isDone()==1);
        ejecutar.setEnabled(!ejecutar.isChecked());
        ejecutar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<ResponseBody> ejecutarCall = service.Ejecutar(a.getId());
                ejecutarCall.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful())
                        {
                            Log.i("TAG", "success ");
                            actividad.SetList();
                        }
                        else
                        {
                            Log.e("TAG", "error " + response.message());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("TAG", "error " + t.getMessage());
                    }
                });
            }
        });

        return convertView;
    }

    public void SetDataSet(List<Actividad> dataset)
    {
        this.dataset = dataset;
        notifyDataSetChanged();
    }
}
