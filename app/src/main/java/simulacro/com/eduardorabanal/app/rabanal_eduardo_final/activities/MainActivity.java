package simulacro.com.eduardorabanal.app.rabanal_eduardo_final.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.R;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.adapters.AdapterLV;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.entities.Actividad;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.service.IService;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.service.ServiceProvider;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    Button btnCrear;

    IService service;
    AdapterLV adapter;
    //android.R.drawable.star_big_off

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        service= ServiceProvider.getService();

        listView = (ListView) findViewById(R.id.listView);
        adapter = new AdapterLV();
        adapter.setContext(getApplicationContext());
        adapter.setActivity(this);
        adapter.setService(service);
        listView.setAdapter(adapter);
        btnCrear = (Button) findViewById(R.id.btnCrear);
        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),CreateActivity.class));
            }
        });

        SetList();
    }

    public void SetList() {
        Call<List<Actividad>> listCall = service.GetActividades("710912");
        listCall.enqueue(new Callback<List<Actividad>>() {
            @Override
            public void onResponse(Call<List<Actividad>> call, Response<List<Actividad>> response) {
                if(response.isSuccessful())
                {
                    Log.i("TAG", "exito");
                    List<Actividad> lista = response.body();
                    adapter.SetDataSet(lista);
                }else
                {

                    Log.e("TAG", "bad request");
                }
            }

            @Override
            public void onFailure(Call<List<Actividad>> call, Throwable t) {
                Log.e("TAG", "error " + t.getMessage());
            }
        });
    }
}
