package simulacro.com.eduardorabanal.app.rabanal_eduardo_final.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.R;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.service.IService;
import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.service.ServiceProvider;

public class CreateActivity extends AppCompatActivity {
    IService service;

    EditText date, activity;
    Button btnCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        service = ServiceProvider.getService();

        //elements
        date = (EditText) findViewById(R.id.date);
        activity = (EditText) findViewById(R.id.activity);
        btnCreate = (Button) findViewById(R.id.btnCreate);

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<ResponseBody> createCall = service.Crear(date.getText().toString(),activity.getText().toString(),"710912");
                createCall.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful())
                        {
                            Log.i("TAG", "success");
                            startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        }else{
                            Log.e("TAG", "bad request");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("TAG", "error");
                    }
                });
            }
        });
    }
}
