package simulacro.com.eduardorabanal.app.rabanal_eduardo_final.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import simulacro.com.eduardorabanal.app.rabanal_eduardo_final.R;

public class MenuActivity extends AppCompatActivity {
    Button listar, crear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        listar= (Button) findViewById(R.id.btnListar);
        listar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });

        crear= (Button) findViewById(R.id.btnCrear);
        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),CreateActivity.class));
            }
        });
    }
}
