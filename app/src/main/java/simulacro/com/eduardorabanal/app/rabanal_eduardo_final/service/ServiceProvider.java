package simulacro.com.eduardorabanal.app.rabanal_eduardo_final.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 12/07/2017.
 */
public class ServiceProvider {
    public static IService getService()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://34.211.47.39/actividades/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(IService.class);
    }
}
